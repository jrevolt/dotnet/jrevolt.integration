using System.CommandLine;
using System.Reflection;
using Autofac;
using Autofac.Core;
using JRevolt.Configuration.CommandLine;
using JRevolt.Injection;

namespace JRevolt.Integration
{
   [Service]
   internal class CliProvider : IDynamicServiceResolver<Command>
   {
      [Inject] private ILifetimeScope Scope { get; }

      public Command? Command { get; private set; }

      public void Resolve(object target, IComponentContext ctx, IComponentRegistration registration, MemberInfo member, out object result)
      {
         result = Command ??= ResolveCommand();
      }

      private Command ResolveCommand()
      {
         var assemblies = Scope.Resolve<IApplicationAssembliesProvider>().GetApplicationAssemblies();
         var cli = new CliBuilder(assemblies)
            .WithCliCommandFactory(Scope.Resolve)
            .Build();
         var opt = new Option<string>("-p")
         {
            Arity = ArgumentArity.ZeroOrMore,
            Description = "Configuration properties in key=value format",
            AllowMultipleArgumentsPerToken = true,
         };
         opt.AddAlias("--properties");
         cli.AddGlobalOption(opt);
         return cli;
      }
   }
}
