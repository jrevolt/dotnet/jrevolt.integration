using System;
using System.CommandLine;
using System.CommandLine.Parsing;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Core;
using JRevolt.Configuration;
using JRevolt.Injection;
using Microsoft.Extensions.Configuration;

namespace JRevolt.Integration
{
   [Service]
   internal class ConfigurationProvider : IDynamicServiceResolver<IConfiguration>
   {
      [Inject] private ILifetimeScope Scope { get; }

      public IConfiguration? Configuration { get; private set; }

      public void Resolve(object target, IComponentContext ctx, IComponentRegistration registration, MemberInfo member, out object result)
      {
         result = Configuration ??= ResolveConfiguration();
      }

      private IConfiguration ResolveConfiguration()
      {
         var assembly = Scope.Resolve<IApplicationAssembliesProvider>().GetApplicationAssemblies().First();
         var cmd = Scope.Resolve<CommandProvider>().Command;

         string[] args = Environment.GetCommandLineArgs().Skip(1).ToArray();

         var opt = cmd.GlobalOptions.First(x => x.HasAlias("-p"));
         var cli = cmd.Parse(args);
         var vals = cli.FindResultFor(opt)?.GetValueOrDefault<string[]>() ?? Array.Empty<string>();

         var cfgprops = vals
            .Select(x => x.Split('=', 2))
            .Where(x => x.Length==2)
            .ToDictionary(x => x[0], x => x[1], StringComparer.OrdinalIgnoreCase);

         return new ConfigurationLoader(assembly)
            .UseCommandLine(cfgprops)
            //.EnableReferences()
            .Load()
            .Build();
      }
   }

   [Service]
   internal class CommandProvider
   {
      [Inject] internal Command Command { get; }
   }

}
