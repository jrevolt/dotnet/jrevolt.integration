using System.Reflection;

namespace JRevolt.Integration
{
   public interface IApplicationAssembliesProvider
   {
      Assembly[] GetApplicationAssemblies();
   }

   public class ApplicationAssembliesProvider : IApplicationAssembliesProvider
   {
      private Assembly[] assemblies;

      public ApplicationAssembliesProvider(params Assembly[] assemblies) => this.assemblies = assemblies;

      public Assembly[] GetApplicationAssemblies() => assemblies;
   }
}
