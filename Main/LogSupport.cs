using System;
// using Microsoft.AspNetCore.Hosting;
using Serilog;
using Serilog.Configuration;
using Serilog.Debugging;
using Serilog.Events;

namespace JRevolt.Integration
{
   public static class LogSupport
   {
      public static void ConfigureBootLogging(Func<LoggerMinimumLevelConfiguration, LoggerConfiguration>? configure = null)
      {
         var sLogLevel = Environment.GetEnvironmentVariable("SERILOG");
         if (!Enum.TryParse<LogEventLevel>(sLogLevel, out var logLevel)) logLevel = LogEventLevel.Warning;
         if (logLevel <= LogEventLevel.Debug) SelfLog.Enable(Console.WriteLine);
         var template = "[{Timestamp:HH:mm:ss}|{Level:u3}|{SourceContext}] {Message:lj} {NewLine}{Exception}";
         Log.Logger = new LoggerConfiguration()
            .WriteTo.Console(logLevel, template, standardErrorFromLevel: LogEventLevel.Verbose)
            .MinimumLevel.Is(logLevel)
            .MinimumLevel.Override("JRevolt.Injection.Injector+Locking", LogEventLevel.Information)
            .CreateBootstrapLogger();
      }

      // public static void ConfigureLogging(IWebHostBuilder ctx, LoggerConfiguration logcfg)
      // {
      //    var log = Log.ForContext(typeof(LogSupport));
      //    log.Information("Configuring logging...");
      //    logcfg.ReadFrom.Configuration(ctx.Configuration);
      // }
   }
}
