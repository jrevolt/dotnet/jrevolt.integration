using System.Reflection;
using Autofac;
using Autofac.Core;
using JRevolt.Injection;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace JRevolt.Integration
{
   [Service]
   internal class LoggerInjector : IDynamicServiceResolver<ILogger>
   {
      [Inject] private IConfiguration Configuration { get; }

      private ILogger? log;

      private ILogger Logger()
      {
         if (log != null) return log;

         Log.Debug("Configuring logging...");

         return log ??= Log.Logger = new LoggerConfiguration() //NOSONAR
            .ReadFrom.Configuration(Configuration)
            .CreateLogger();
      }

      public void Resolve(object target, IComponentContext ctx, IComponentRegistration registration, MemberInfo member, out object result)
      {
         result = Logger().ForContext(target.GetType());
         //result = Log.ForContext(registration.Activator.LimitType);
      }
   }
}
