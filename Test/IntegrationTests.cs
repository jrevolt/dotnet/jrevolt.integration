using System;
using System.Reflection;
using Autofac;
using Autofac.Core;
using FluentAssertions;
using JRevolt.Injection;
using JRevolt.Injection.Testing;
// using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using Serilog;
using Serilog.Events;

namespace JRevolt.Integration.Test
{
   public class IntegrationTests
   {
      [Theory]
      public void LogSupport_BootLogging()
      {
         Environment.SetEnvironmentVariable("SERILOG", "Debug");
         LogSupport.ConfigureBootLogging(configuration => configuration.Override("dummy", LogEventLevel.Debug));
         Log.Logger.GetType().Name.Should().Contain("Reloadable");
      }

      // [Theory]
      // public void LogSupport_ConfigureLogging()
      // {
      //    var ctx = new WebHostBuilderContext {Configuration = new ConfigurationBuilder().Build()};
      //    LogSupport.ConfigureLogging(ctx, new LoggerConfiguration());
      // }

      [Theory]
      public void CliProvider()
      {
         void SetupAssembliesProvider(Mock<IApplicationAssembliesProvider> mock) =>
            mock.Setup(x =>
               x.GetApplicationAssemblies()).Returns(new[] {Assembly.GetExecutingAssembly()});

         new TestBuilder()
            .WithMock<IApplicationAssembliesProvider>(SetupAssembliesProvider)
            .WithComponent<CliProvider>()
            .Run<CliProvider>(cli =>
         {
            cli.Resolve(new object(), Mock.Of<IComponentContext>(), Mock.Of<IComponentRegistration>(), Mock.Of<MemberInfo>(),
               out var result);
            result.Should().NotBeNull();
         });
      }

      [Theory]
      public void ConfigurationProvider()
      {
         var meta = new Metadata(new TestApplicationAssembliesProvider().GetApplicationAssemblies());
         using var container = new ContainerBuilder()
            .UseInjector(meta)
            .Build();
         container.Resolve<ConfigurationHolder>().Should().NotBeNull();

         // void SetupAssembliesProvider(Mock<IApplicationAssembliesProvider> mock) =>
         //    mock.Setup(x =>
         //       x.GetApplicationAssemblies()).Returns(assemblies);
         //
         // var meta = new Metadata().Scan(assemblies);
         // new TestBuilder(meta)
         //    //.WithMock<IApplicationAssembliesProvider>(SetupAssembliesProvider)
         //    //.WithComponent<CliProvider>()
         //    //.WithComponent<ConfigurationProvider>()
         //    .Run<ConfigurationProvider>(p =>
         //    {
         //       p.Resolve(
         //          new object(),
         //          Mock.Of<IComponentContext>(), Mock.Of<IComponentRegistration>(), Mock.Of<MemberInfo>(),
         //          out var result);
         //       result.Should().NotBeNull();
         //    });
      }

      [Theory]
      public void ApplicationAssembliesProviderTest()
      {
         var provider = new ApplicationAssembliesProvider(Assembly.GetExecutingAssembly());
         provider.GetApplicationAssemblies().Should().Contain(Assembly.GetExecutingAssembly());
      }

      [Theory]
      public void LoggerInjector()
      {
         using var c = new ContainerBuilder()
            .UseInjector(GetType().Assembly, typeof(Module).Assembly)
            .Build();
         c.Resolve<LoggerHolder>().Log.Should().NotBeNull();
      }
   }

   [Service]
   internal class TestApplicationAssembliesProvider : IApplicationAssembliesProvider
   {
      public Assembly[] GetApplicationAssemblies() => new[]
      {
         GetType().Assembly,
         typeof(Integration.Module).Assembly,
      };
   }

   [Service]
   internal class ConfigurationHolder
   {
      [Inject] internal IConfiguration Configuration { get; }
   }

   [Service]
   internal class LoggerHolder
   {
      [Inject] internal ILogger Log { get; }
   }

}
