﻿using System.CommandLine;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.AttributeExtensions;
using JRevolt.Injection;

namespace JRevolt.Integration.TestConsoleApp
{
   [SingleInstance]
   internal class Program : IApplicationAssembliesProvider
   {
      [Inject] private Command Command { get; }

      internal static async Task<int> Main(string[] args)
      {
         LogSupport.ConfigureBootLogging();
         await using var container = new ContainerBuilder().UseInjector(Assemblies).Build();
         return await container.Resolve<Program>().Command.InvokeAsync(args);
      }

      private static Assembly[] Assemblies => new[]
      {
         typeof(Program).Assembly,
         typeof(JRevolt.Integration.Module).Assembly,
      };

      public Assembly[] GetApplicationAssemblies() => Assemblies;

   }
}
