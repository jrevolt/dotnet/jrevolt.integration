﻿using JRevolt.Configuration.CommandLine;
using JRevolt.Injection;
using Serilog;

namespace JRevolt.Integration.TestConsoleApp
{
   [Service]
   [CliCommand]
   public class SampleCommand
   {
      [Inject] private ILogger Log { get; }

      [CliEntrypoint]
      internal void Run()
      {
         Log.Information("Hello, World!");
      }

   }
}
