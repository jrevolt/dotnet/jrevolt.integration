﻿using System;
using System.Threading.Tasks;
using JRevolt.Configuration.AspNetCore;
using JRevolt.Configuration.CommandLine;
using JRevolt.Injection;
using JRevolt.Injection.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;

namespace JRevolt.Integration.TestWebApiApp;

[Component]
[CliCommand(Root = true)]
internal class RootCommand
{
   [Inject] private IConfiguration Cfg { get; }

   [Inject] private Injector Injector { get; }

   [CliEntrypoint]
   internal async Task Run()
   {
      var builder = WebApplication.CreateBuilder();
      builder.Host.UseServiceProviderFactory(new InjectorServiceProviderFactory(Injector.Metadata));
      builder.Host.ConfigureAppConfiguration(Cfg.ConfigureAppConfiguration);
      // builder.WebHost.ConfigureAppConfiguration((ctx, cb) => { return; });
      // builder.WebHost.UseSetting("foo", "bar");
      builder.WebHost.UseSerilog((ctx, logcfg) =>
      {
         logcfg.ReadFrom.Configuration(ctx.Configuration);
      });
      //builder.WebHost.UseConfiguredEndpoint(Cfg, false);


      //builder.Services.AddControllers();
      builder.Services.AddEndpointsApiExplorer().AddSwaggerGen();
      builder.Services.AddInjectorServices();
      var app = builder.Build();
      app.UseConfiguration();
      app.UseSwagger();
      app.UseSwaggerUI();
      //app.MapControllers();
      app.UseInjector();
      await app.RunAsync();
   }

}

[Component]
[CliCommand]
internal class ConfigCommand : RootCommand
{
   [CliEntrypoint]
   internal void Run()
   {
   }
}
