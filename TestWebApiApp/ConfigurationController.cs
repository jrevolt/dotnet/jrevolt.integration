﻿using System.Text;
using JRevolt.Injection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace JRevolt.Integration.TestWebApiApp
{
   [Service]
   [ApiController]
   [Route("[controller]")]
   internal class ConfigurationController
   {
      [Inject] private IConfiguration Cfg { get; }


      [HttpGet]
      public IActionResult Index()
      {
         var buf = new StringBuilder();
         foreach (var (k,v) in Cfg.AsEnumerable())
         {
            buf.Append(k).Append("=").Append(v).Append("\n");
         }

         return new OkObjectResult(buf.ToString());
      }
   }
}
