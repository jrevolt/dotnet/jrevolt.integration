using System.CommandLine;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.AttributeExtensions;
using JRevolt.Injection;
using Microsoft.Extensions.Configuration;

namespace JRevolt.Integration.TestWebApiApp;

[SingleInstance]
internal class Program : IApplicationAssembliesProvider
{
   [Inject] private Command Command { get; }

   [Inject] private IConfiguration Cfg { get; } // force load

   internal static async Task<int> Main(string[] args)
   {
      LogSupport.ConfigureBootLogging();
      Utils.ReportVersions();
      await using var container = new ContainerBuilder().UseInjector(Assemblies).Build();
      return await container.Resolve<Program>().Command.InvokeAsync(args);
   }

   private static Assembly[] Assemblies => new[]
   {
      typeof(Module).Assembly,
      typeof(Integration.Module).Assembly,
   };

   public Assembly[] GetApplicationAssemblies() => Assemblies;

}
