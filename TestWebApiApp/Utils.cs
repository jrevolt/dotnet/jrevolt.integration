﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Serilog;

namespace JRevolt.Integration.TestWebApiApp
{
   public static class Utils
   {
      internal static Assembly GetAssembly(string name)
      {
         var found = AppDomain.CurrentDomain
            .GetAssemblies()
            .SelectMany(x => x.GetReferencedAssemblies())
            .Distinct()
            .First(x => x.Name!.Equals(name));
         return Assembly.Load(found);
      }

      internal static void ReportVersions()
      {
         Assembly.GetEntryAssembly()!.GetReferencedAssemblies()
            .Append(Assembly.GetExecutingAssembly().GetName())
            .Distinct()
            //.Where(x => x.Name.StartsWith(nameof(JRevolt)))
            .Select(x => Assembly.Load(x))
            .ToImmutableSortedSet(Comparer<Assembly>.Create((a, b) => a.GetName().Name.CompareTo(b.GetName().Name)))
            .ToList()
            .ForEach(ReportVersion)
            ;
      }

      internal static void ReportVersion(Assembly assembly)
      {
         var info = FileVersionInfo.GetVersionInfo(assembly.Location);
         Log.ForContext(typeof(Utils)).Verbose("Assembly Name={0} Version={1}", info.FileDescription, info.ProductVersion);
      }
   }
}
