﻿using JRevolt.Injection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace JRevolt.Integration.TestWebApiApp
{
   [Service]
   [ApiController]
   [Route("[controller]")]
   internal class VersionController
   {
      [Inject] private ILogger Log { get; }
      [Inject] private IConfiguration Cfg { get; }

      [HttpGet]
      public IActionResult Index()
      {
         return new OkObjectResult(new { version = "1.0" });
      }
   }
}
